import { Component } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import{HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AppComponent {
  
  title = 'dashboard';
  columnsToDisplay = ['BusNumber','Destination','Occupancy','Capacity' ,'Request to board'];
  columnsToDisplay_ : ColoumnDetail[] = 
  [{Name:'BusNumber',Id:'BusNumber'},
  {Name:'Destination',Id:'Towards'},
  {Name:'Occupancy',Id:'Occupancy'},
  {Name:'Capacity',Id:'Capacity'},
  {Name:'Request to board', Id:'StopRequest'}
];
  expandedElement: BusListModel | null;

  readonly ROOT_URL='';
  posts: any;
  constructor(private http:HttpClient){
    this.getPosts();
  }
  getPosts(){
    return  this.http.get('http://10.1.8.9:8080/bus/list').subscribe(
          (response) => {
            console.log(response)
            this.dataSource = response as BusListModel[];
            location.reload();
          },
          (error) => {
            console.log(error)
          }
        )

  }

 
}

export interface ColoumnDetail{
  Name:string;
  Id:string;
}

export interface BusListModel {  
  Code: String;
  BusNumber: String;
  PointA: String;
  PointB:String;
  Towards:String;
  NextStop:string;
  StopRequest:String;
  Capacity:Number; 
  Occupancy: Number;
}

const ELEMENT_DATA: BusListModel[] = [
  {
    Code:'',
    BusNumber: '150A',
    PointA:'City Center',
    PointB:'Kildare',
    Towards:'Kiladre',
    NextStop:'CollegeGreen',
    StopRequest:'Yes',
    Capacity:50,
    Occupancy:40
  }
  
];